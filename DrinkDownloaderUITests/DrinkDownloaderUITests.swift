//
//  DrinkDownloaderUITests.swift
//  DrinkDownloaderUITests
//
//  Created by Nicky Taylor on 10/24/22.
//

import XCTest

final class DrinkDownloaderUITests: XCTestCase {

    func testExample() throws {
        
        // 1
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
        
        
        XCUIDevice.shared.orientation = .portrait
        snapshot("hello_snapshot")
        
        XCUIDevice.shared.orientation = .landscapeLeft
        snapshot("hello_snapshot_ls")
        
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
    
    
}
